<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;

class ListPosts extends Component
{
    use WithPagination;

    protected $paginationTheme = 'tailwind';

    public $search;

    protected $queryString = ['search' => ['except' => ''],];

    public function render()
    {
        return view('livewire.list-posts', [
            'posts' => Post::where('title', 'like', '%'.$this->search.'%')->paginate(5),
        ]);
    }
}
