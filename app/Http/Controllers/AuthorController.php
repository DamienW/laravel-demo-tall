<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function view(Author $author)
    {
        return view('author.view', ['author'=>$author]);
    }
}
