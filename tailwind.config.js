module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
        "./vendor/**/*.blade.php",
    ],
    theme: {
        extend: {},
    },
    plugins: [],
};
