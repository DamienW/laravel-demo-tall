@extends('app')

@section('body')
  <h1 class="font-semibold text-2xl mb-4">Laravel demo TALL</h1>
  @livewire('list-posts')
@endsection