<nav id="Navbar" data-turbolinks-permanent class="fixed top-0 w-full h-12 bg-white shadow-md">
  <ul class="container mx-auto flex items-center justify-start h-full">
    <li><a href="{{route('home.index')}}" class="text-blue-500 hover:text-blue-800">Home</a></li>
  </ul>
</nav>