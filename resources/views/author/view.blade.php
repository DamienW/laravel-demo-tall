@extends('app')

@section('body')
  <h1 class="font-semibold text-2xl mb-4">Author name : {{$author->name}}</h1>
  <h2>Posts List :</h2>
  <ul class="divide-y">
    @foreach ($author->posts as $post)
      <li class="py-2"><a href="{{route('post.view', $post->slug)}}" class="text-blue-500 hover:text-blue-800">{{$post->title}}</a></li>
    @endforeach
  </ul>
@endsection