<div class="bg-white shadow-md rounded p-4">
    <div class="flex flex-row items-center justify-between">
        <h2 class="font-semibold text-xl mb-2">Posts list :</h2>
        <input wire:model="search" type="search" placeholder="Search posts by title..." class="border border-gray-200 rounded py-1 px-2">
    </div>
    <ul class="divide-y my-3 container mx-auto">
        @foreach ($posts as $post)
        <li class="py-2 grid grid-cols-4">
        <div class="col-span-3 flex flex-col items-start justify-center">
            <h2><a href="{{route('post.view', $post->slug)}}" class="text-blue-500 hover:text-blue-800">{{$post->title}}</a></h2>
            <p>{{$post->description}}</p>
        </div>
        <div class="col-span-1 flex flex-col items-end justify-center">
            @if ($post->author)
            <p class="text-xs">créé par : <a href="{{route('author.view', $post->author->slug)}}" class="text-blue-500 hover:text-blue-800">{{$post->author->name}}</a></p>
            <small class="text-xs italic">créé le : {{$post->created_at->format('d/m/Y')}}</small>
            @endif
        </div>
        </li>
        @endforeach
    </ul>
    {{ $posts->links() }}
</div>
