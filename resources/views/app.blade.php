<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laravel TALL</title>

  @yield('meta')

  @livewireStyles
  <link rel="stylesheet" href="{{mix('css/app.css')}}" data-turbolinks-track="reload">
  <script src="{{mix('js/app.js')}}" data-turbolinks-track="reload" defer></script>
</head>
<body>
  
  @include('includes/navBar')

  <main role="main" class="min-h-screen bg-slate-100 pt-12">
    <div class="container mx-auto py-4">
      @yield('body')
    </div>
  </main>

  @livewireScripts
  <script src="{{mix('js/livewire-turbolinks.js')}}"  data-turbolinks-eval="false" data-turbo-eval="false"></script>
</body>
</html>