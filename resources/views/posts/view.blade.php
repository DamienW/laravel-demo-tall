@extends('app')

@section('body')
<div class="container mx-auto">
  <h1 class="font-semibold text-2xl">{{$post->title}}</h1>
  <p>{{$post->description}}</p>
</div>
@endsection