const mix = require("laravel-mix");
require("laravel-mix-artisan-serve");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .js("resources/js/livewire-turbolinks.js", "public/js/")
    .postCss("resources/css/app.css", "public/css", [require("tailwindcss")]);

if (mix.inProduction()) {
    mix.version().options({
        terser: {
            terserOptions: {
                compress: {
                    drop_console: true,
                },
            },
        },
    });
} else {
    const host = "127.0.0.1";
    const port = 8027;
    const hostBS = "http://" + host + ":" + port;
    const portBS = port - 1;
    const portDS = port + 1;

    mix.serve({
        port: `${port}`,
    })
        .browserSync({
            proxy: `${hostBS}`,
            port: portBS,
            files: ["./resources/views/**/*.blade.php"],
            snippetOptions: {
                rule: {
                    match: /<\/head>/i,
                    fn: function (snippet, match) {
                        return snippet + match;
                    },
                },
            },
        })
        .options({
            hmrOptions: {
                host: `${host}`,
                port: `${portDS}`,
            },
        });
}
