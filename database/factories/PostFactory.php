<?php

namespace Database\Factories;

use App\Models\Author;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title'       => $this->faker->sentence(),
            'slug'        => $this->faker->slug(),
            'description' => $this->faker->text(rand(70,150)),
            'author_id'   => Author::inRandomOrder()->first()->id
        ];
    }
}
